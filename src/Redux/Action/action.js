import { DELETE_USER_INFOR, SET_USER_INFOR } from "../Contain/contain";

export const setUserInfoAction = (payload) => {
  return {
    type: SET_USER_INFOR,
    payload: payload,
  };
};
