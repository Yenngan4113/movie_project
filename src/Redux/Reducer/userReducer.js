import { localStorageServ } from "../../services/localServices";
import { DELETE_USER_INFOR, SET_USER_INFOR } from "../Contain/contain";

const initialState = {
  userInfor: localStorageServ.getUserInfor(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_INFOR: {
      state.userInfor = payload;
      return { ...state };
    }

    default:
      return state;
  }
};
