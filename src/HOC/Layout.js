import React from "react";
import HeaderTheme from "../Component/HeaderTheme/HeaderTheme";

export default function LayoutTheme({ Component }) {
  return (
    <div>
      <HeaderTheme />
      <Component />
    </div>
  );
}
