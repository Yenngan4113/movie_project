import DetailPage from "../Page/DetailPage/DetailPage";
import Homepage from "../Page/Homepage/Homepage";
import LoginPage from "../Page/LoginPage/LoginPage";
import RegisterPage from "../Page/RegisterPage/RegisterPage";
import LayoutTheme from "../HOC/Layout";

export const userRoutes = [
  {
    path: "/",
    component: <LayoutTheme Component={Homepage} />,
    exact: true,
    isUseLayout: true,
  },
  {
    path: "/login",
    component: LoginPage,
  },
  {
    path: "/register",
    component: RegisterPage,
  },
  {
    path: "/detail/:id",
    component: <LayoutTheme Component={DetailPage} />,
    isUseLayout: true,
  },
];
