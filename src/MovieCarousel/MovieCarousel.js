import { Carousel } from "antd";
import MovieItem from "../MovieItem/MovieItem";

const MovieCarousel = ({ chunkedList }) => {
  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };

  return (
    <Carousel afterChange={onChange}>
      {chunkedList.map((movie, index) => {
        return (
          <div className=" h-max w-full bg-blue-600" key={index}>
            <div className="grid grid-cols-4">
              {movie.map((item) => {
                return <MovieItem />;
              })}
            </div>
          </div>
        );
      })}
    </Carousel>
  );
};

export default MovieCarousel;
