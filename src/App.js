import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Homepage from "./Page/Homepage/Homepage";
import { userRoutes } from "./Route/userRoutes";

function App() {
  return (
    <div className="">
      <BrowserRouter>
        <Switch>
          {userRoutes.map((route, index) => {
            if (route.isUseLayout) {
              return (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  render={() => {
                    return route.component;
                  }}
                />
              );
            }
            return (
              <Route
                key={index}
                path={route.path}
                component={route.component}
                exact={route.exact}
              />
            );
          })}
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
