import { Card } from "antd";
const { Meta } = Card;

const MovieItem = ({ Movie }) => {
  return (
    <Card
      hoverable
      style={{
        width: "100%",
        padding: "50px",
        border: "none",
      }}
      cover={
        <img
          alt="example"
          src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"
        />
      }
    >
      <Meta title="Europe Street beat" description="www.instagram.com" />
    </Card>
  );
};

export default MovieItem;
