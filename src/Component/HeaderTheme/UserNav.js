import { useSelector, useDispatch } from "react-redux";
import React from "react";
import { NavLink } from "react-router-dom";
import { localStorageServ } from "../../services/localServices";
import { deleteUserInfoAction } from "../../Redux/Action/action";

export default function UserNav() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  //   let dispatch = useDispatch();
  return (
    <div>
      {userInfor ? (
        <div>
          <span className="mr-3 font-bold text-base w-24 inline-block mr-3">
            {" "}
            {userInfor?.hoTen}
          </span>
          <button
            className="rounded bg-red-600 text-white py-1 px-2"
            onClick={() => {
              localStorageServ.removeUserInfor();
              window.location.href = "/login";
            }}
          >
            Đăng xuất
          </button>
        </div>
      ) : (
        <div>
          <NavLink to="/login">
            <button className="rounded bg-yellow-400 text-white mr-3 py-1 px-2 w-24">
              Đăng nhập
            </button>
          </NavLink>
          <button className="rounded bg-green-600 text-white py-1 px-2">
            Đăng ký
          </button>
        </div>
      )}
    </div>
  );
}
