import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function HeaderTheme() {
  return (
    <div className=" h-12 w-full flex items-center justify-between shadow-lg px-12">
      <NavLink to="/">
        <span className="font-bold text-purple-800 text-lg">Logo</span>
      </NavLink>
      <div>
        <NavLink to="/">
          <span className="font-medium text-green-400 text-lg mr-6">News</span>
        </NavLink>
        <NavLink to="/">
          <span className="font-medium text-green-400 text-lg mr-6">
            Movie Theater
          </span>
        </NavLink>
        <NavLink to="/">
          <span className="font-medium text-green-400 text-lg mr-6">
            Schedule
          </span>
        </NavLink>
      </div>
      <UserNav />
    </div>
  );
}
