import React from "react";
import Login from "./FormLogin/Login";

export default function LoginPage() {
  return (
    <div className=" w-screen h-screen bg-green-600 flex items-center">
      <div className="flex w-full h-full">
        <div className="w-1/2"></div>
        <div className="w-1/2 mx-auto p-5 rounded bg-white flex content-center ">
          <div className="w-full h-max">
            <Login />
          </div>
        </div>
      </div>
    </div>
  );
}
