import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userService } from "../../../services/userServices";
import { localStorageServ } from "../../../services/localServices";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setUserInfoAction } from "../../../Redux/Action/action";

export default function Login() {
  // useHistory allows to change URL
  let history = useHistory();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    userService
      .postDangNhap(values)
      .then((res) => {
        localStorageServ.setUserInfor(res.data.content);
        dispatch(setUserInfoAction(res.data.content));
        message.success("Đăng nhập thành công");
        // setTimeout: after 2s,history starts
        // setInterval: after 2s, history will repeat forever
        setTimeout(() => {
          history.push("/");
        }, 2000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Form
      name="basic"
      layout="vertical"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="taiKhoan"
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="matKhau"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <div className="text-center">
        <Button
          type="primary"
          htmlType="submit"
          className=" bg-blue-700 text-white mx-auto"
        >
          Submit
        </Button>
      </div>
    </Form>
  );
}
