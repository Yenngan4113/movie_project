import _ from "lodash";
import React, { useEffect, useState } from "react";
import MovieCarousel from "../../MovieCarousel/MovieCarousel";
import { movieService } from "../../services/movieServices";
import "./homepage.css";

export default function Homepage() {
  const [movieList, setMovieList] = useState([]);
  useEffect(() => {
    let fetchMovieList = async () => {
      let result = await movieService.getMovieList();
      let chunkedList = _.chunk(result.data.content, 8);
      setMovieList(chunkedList);
    };
    fetchMovieList();
  }, []);
  return (
    <div>
      <div id="carousel_homepage" className="container m-auto">
        <MovieCarousel chunkedList={movieList} />
      </div>
    </div>
  );
}
